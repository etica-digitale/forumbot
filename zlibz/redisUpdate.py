# SPDX-FileCopyrightText: 2021 Andrea Laisa <amreo linux it>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from zlibz.flarumPublicAPI import flarum_public_api as flarum
from zlibz.zutils import rdb, url_domain, rcli
from markdownify import markdownify
import re
import time


def isBool(data):
    if data:
        return 1
    return 0

def toBool(data):
    if data == '0':
        return False
    elif data == '1':
        return True


def have_emoji(name):
    moji_list = {
                    'Annunci'             : '📣',
                    'Articoli ED'         : '📰',
                    'Generale'            : '💬',
                    'Mondo'               : '🌐',
                    'Sanità'              : '🩸',
                    'Scuola e istruzione' : '🏫', 
                    'Intrattenimento'     : '🎭',
                    'Social'              : '🗣',
                    'Telefonia'           : '📱',
                    'GNU/Linux'           : '🐧',
                    'Software'            : '💡',
                    'Win/OS-X'            : '💻',
                    'Altro'               : '💾',
                }

    if name in moji_list:
        return name + ' ' + moji_list[name]
    else:
        return name


def getContent(content):
    xy = re.compile(r'\!\[.+\]\(.+\)')
    xyx = re.compile(r'\!\[\]\(.+\)')
    xx = re.compile(r'\(.+\)')

    if xy.findall(content):
        pic_url = xx.findall(xy.findall(content)[0])[0][1:-1]
    elif xyx.findall(content):
        pic_url = xx.findall(xyx.findall(content)[0])[0][1:-1]
    else:
        pic_url = 0

    return xy.sub('', content), pic_url


def isPost(post):
    content, pic_url = getContent(markdownify(post.attributes.contentHtml))
    if 'relationships' in vars(post):
        rdb.hmset('post:data:%s'% post.id, {
                            'thread'    : post.relationships.discussion.data.id,
                            'main_user' : post.relationships.user.data.id,
                            'content'   : content,
                            'pic_url'   : pic_url
                 })
    else:
        rdb.hmset('post:data:%s'% post.id, {
                        'content' : content,
                        'pic_url' : pic_url
                 })

    return True


def isTag(tag, homepage):
    name = tag.attributes.name 
    if tag.attributes.isChild and 'relationships' in vars(tag):
        parent_type = tag.relationships.parent.data.type 
        parent_id   = tag.relationships.parent.data.id
        name = have_emoji(name)
    else:
        parent_type = isBool(False)
        parent_id   = isBool(False)
    homepage_flag = False
    if tag.id in homepage:
        homepage_flag = True
    elif not tag.attributes.isChild:
        name = '#'+tag.attributes.name

    url = "%s/t/%s"%(url_domain, tag.attributes.slug)
    rdb.hmset('tag:data:%s'%tag.id, {
                        'name'        : name,
                        'slug'        : tag.attributes.slug,
                        'url'         : url,
                        'child'       : isBool(tag.attributes.isChild),
                        'hidden'      : isBool(tag.attributes.isHidden),
                        'postCount'   : tag.attributes.postCount,
                        'parent_type' : parent_type,
                        'parent_type' : parent_id, 
                        'homepage'    : isBool(homepage_flag),
                                            }
                    )
    return True

def isUser(user, extra=False): #extra vale a dire by api/post/post_id
    if extra: 
        rdb.hset('user:data:%s'%user.id, 'groups', ':'.join([x.id for x in user.relationships.groups.data]))
    rdb.hmset('user:data:%s'%user.id, {
                            'name' : user.attributes.displayName,
                            'username' : user.attributes.username,
                            'avatar' : str(user.attributes.avatarUrl),
                                    }
                                    )
    return True

def isGroup(group):
    rdb.hmset('group:data:%s'% group.id, {
                            'name'  : group.attributes.nameSingular,
                            'names' : group.attributes.namePlural
                                            }
                    )
    return True


def checkThread(thread):
    if not rdb.exists('thread:data:%s'%thread.id):
        return True
    return False

def isThread(thread, extra=False, send_new=False):
    new_flag = checkThread(thread)
    tags = ':'.join([x.id for x in thread.relationships.tags.data])
    url = "%s/d/%s"%(url_domain, thread.attributes.slug)
    is_new = False
    if checkThread(thread):
        is_new = True
        update_single_post(thread.relationships.lastPost.data.id)
    elif not thread.relationships.lastPost.data.id == rdb.hget('thread:data:%s'%thread.id, 'last_post_id') and send_new: 
        if update_single_post(thread.relationships.lastPost.data.id):
                rdb.set('post:new:%s'%thread.relationships.lastPost.data.id, thread.id)
                print('new post')
        
    if not extra:
        if new_flag: 
            rdb.hmset('thread:data:%s'%thread.id, {
                            'main_user' : thread.relationships.user.data.id, # data.type = users
                            'main_post' : thread.relationships.firstPost.data.id, # data.type = posts
                                                 }
                    )
        if not rcli.redis.exists(thread.id):
            rcli.add_document(thread.id,
                          title = thread.attributes.title,
                          url = url,
                          main_post = thread.relationships.firstPost.data.id, 
                          main_user = thread.relationships.user.data.id,
                        )
    rdb.hmset('thread:data:%s'%thread.id, {
                            'title'           : thread.attributes.title,
                            'slug'            : thread.attributes.slug,
                            'url'             : url,
                            'comment_count'   : thread.attributes.commentCount,
                            'last_post_id'    : thread.relationships.lastPost.data.id, # ?
                            'last_post_date'  : thread.attributes.lastPostedAt,
                            'approved'        : isBool(thread.attributes.isApproved),
                            'tags'            : tags,
                            'created_date'    : thread.attributes.createdAt,
                            'pic_url'         : ''
                            #'from_user' : thread.relationships.user.data.id
                            #'tg_sub'    : "x", # se esiste da non cambiare
                                                }
                    )
    if send_new and is_new:
            rdb.set('thread:new:%s'%thread.id, tags)
    return True

def update_all_threads(url=False, new=False):
    if url:
        th = flarum.getThreads(False, url=url)
    else:
        th = flarum.getThreads(False)
    if not th:
        return False
    for thread in th.data:
        if thread.type == 'discussions':
            isThread(thread, send_new=new)
            rdb.lpush('threads:update:list', 'thread:data:%s'%thread.id) 
    for inc in th.included:
        if inc.type == 'users':
            isUser(inc)
        elif inc.type == 'posts':
            isPost(inc)

    if 'next' in vars(th.links):
        update_all_threads(url=th.links.next, new=new)
    else: 
        live_up = rdb.lrange('threads:update:list', 0, -1)
        thread_data = rdb.keys('thread:data:*')
        for thread in thread_data:
            if not thread in live_up and rdb.exists(thread):
                rdb.delete(thread)
        rdb.delete('threads:update:list')
    return True

def update_all_tags():
    tg = flarum.getTags(False)
    if not tg:
        print('errore')
        return False
    homepage = [tag.id for tag in tg.included]
    for tag in tg.data:
        if tag.type == 'tags':
            isTag(tag, homepage)
    return True

## serve? mi sa di no
def update_user_from_post(extra):
    pst = flarum.getPosts(extra)
    if not pst:
        print('errore')
        return False
    for user in dth.users:
        if user.type == 'users':
            isUser(user)
    return True

def update_post_from_thread(extra):
    dth = flarum.getThreads(extra)
    if not dth:
        print('errore')
        return False
    for post in dth.included:
        if post.type == 'posts' and post.attributes.contentType == 'comment': # vedere che eccezzioni crea
            isPost(post)
        elif post.type == 'users':
            isUser(post)
    return True

def update_all_posts_from_threads():
    for thread in rdb.keys('thread:data:*'):
        th_id = thread.split(':')[2]
        if not update_post_from_thread(th_id):
            print('errore')
            return False
        time.sleep(0.3)
        print('posts of thread number: '+ th_id + ' done!')
    return True


def update_all_groups():
    grp = flarum.getGroups(False)
    if not grp:
        print('errore')
        return False
    for group in grp.data:
        if group.type == 'groups':
            isGroup(group)
    return True


def update_single_post(extra):
    post = flarum.getPosts(extra)
    if not post:
        print('errore')
        return False
    if post.data.type == 'posts' and post.data.attributes.contentType == 'comment':
        isPost(post.data)
        if post.included[0].type == 'users':
            isUser(post.included[0]) # non prendo included casa
        return True
    return False

