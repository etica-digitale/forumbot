# SPDX-FileCopyrightText: 2021 Andrea Laisa <amreo linux it>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import redis
import configparser
import os
from pyrogram import Client
from redisearch import Client as rClient
from redisearch import TextField, NumericField, Query


if os.path.isfile('config.ini'):
    cfg = configparser.ConfigParser()
    cfg.read('config.ini')
else:
    print("! config.ini not found")
    exit()

rcli = rClient('thread', host=cfg['settings']['redis_host'])
try:
    rcli.info()
except Exception as err:
    print(err)
    rcli.create_index([
                TextField('title'),
                TextField('main_post'),
                TextField('main_user'),
                 ])
    print('+ Index creato')

# data
url_domain = cfg['flarum']['url_domain']
rdb = redis.Redis(host=cfg['settings']['redis_host'], port=6379, db=int(cfg['settings']['redis_db']), decode_responses=True)
bot = Client(session_name=cfg['telegram']['bot_name'],
             bot_token=cfg['telegram']['bot_token'],
             #workers=4,
             #no_updates=False
             )
bot_username = cfg['telegram']['bot_username']
sudo_user = [142271626 # amreoboo
            ]

# update timer
#upd_threads_ttl = 60*5 # 5 min
upd_threads_ttl = 60 * int(cfg['ttl']['update_threads'])
#upd_groups_ttl = 60*60*12
upd_groups_ttl = 60 * int(cfg['ttl']['update_groups'])
#upd_tags_ttl = 60*5
upd_tags_ttl = 60 * int(cfg['ttl']['update_tags'])
#upd_posts_from_thread_ttl = 60*60*6
upd_posts_from_thread_ttl = 60 * int(cfg['ttl']['update_posts'])
