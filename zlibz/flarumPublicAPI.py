# SPDX-FileCopyrightText: 2021 Andrea Laisa <amreo linux it>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# flarum public api, no account required 
import urllib3
import json
from types import SimpleNamespace
from zlibz.zutils import url_domain



pm = urllib3.PoolManager()

path_tags    = '/api/tags'
path_threads = '/api/discussions'
path_posts   = '/api/posts'
path_groups   = '/api/groups'

def ifExtra(extra, path):
    if extra:
       path = '%s/%s'%(path, extra)
    return path
        
def respCheck(resp):
    if resp.status == 200:
        jdata = json.loads(resp.data.decode("utf-8"), object_hook=lambda d: SimpleNamespace(** d))
        if 'errors' in vars(jdata):
            status = jdata.errors[0].status
            code   = jdata.errors[0].code
            print('respCheck False#0')
            return False
        return jdata
    else:
        print('respCheck False#1')
        print(resp.status)
        print(resp.data)
        return False
    return path

class flarum_public_api:
    def getRaw(extra, url, path):
        resp = pm.request('GET', url + ifExtra(extra, path))
        return resp

    def getTags(extra, url=url_domain, path=path_tags):
        resp = pm.request('GET', url + ifExtra(extra, path))
        return respCheck(resp)

    def getThreads(extra, url=url_domain, path=path_threads):
        resp = pm.request('GET', url + ifExtra(extra, path))
        return respCheck(resp)

    def getPosts(extra, url=url_domain, path=path_posts):
        resp = pm.request('GET', url + ifExtra(extra, path))
        return respCheck(resp)
        
    def getGroups(extra=False, url=url_domain, path=path_groups):
        resp = pm.request('GET', url + ifExtra(extra, path))
        return respCheck(resp)

