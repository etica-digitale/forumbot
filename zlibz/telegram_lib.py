# SPDX-FileCopyrightText: 2021 Andrea Laisa <amreo linux it>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from pyrogram.types import ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton, InlineQueryResultArticle, InlineQueryResultPhoto, InputTextMessageContent
from zlibz.zutils import rdb
from zlibz.redisUpdate import toBool

main_text = """Salve! 
Tramite questo bot potrai gestire la ricezione delle **notifiche** relative alle discussioni del Forum di @EticaDigitale.
Oltre ad avere la possibilità tramite i bottoni sottostanti di navigare e iscriverti alle **categorie** ed **etichette** presenti sul Forum per ricevere le notifiche di nuove discussioni, hai la possibilità di ricercare una discussione esistente ed iscriverti ad essa per ricevere notifiche di nuovi post attraverso il bottone 🔎 Cerca.
    """

information_text = "La ricezione delle notifiche non è immediata, di defualt per ogni utente è stato impostato un ritardo di 2h (2 ore), qui puoi modificare l'impostazione\nSe si sceglie **off** le notifiche non saranno comunque in tempo reale ma saranno inviate appena il controllo fatto ogni 5 minuti le troverà."

def getThreadInlineUrl(url):
    return url.split('/')[4].split('-')[0]

def isSubThread(user_id, thread_id):
    if rdb.exists('user:tg:%s'%user_id):
        sub_thread = rdb.hget('user:tg:%s'%user_id, 'sub_thread').split(':')
        if thread_id in sub_thread:
            return InlineKeyboardButton('✅ Notifiche', callback_data='%s:inline_thread_sub:%s'%(thread_id, user_id))
        else:
            return InlineKeyboardButton('❌ Notifiche', callback_data='%s:inline_thread_sub:%s'%(thread_id, user_id))
    else:
        return InlineKeyboardButton('🚫', callback_data='not_in_bot')

def isNotifySubThread(user_id, thread_id, group=False, post_id='none'):
    if rdb.exists('user:tg:%s'%user_id):
        sub_thread = rdb.hget('user:tg:%s'%user_id, 'sub_thread').split(':')
        if thread_id in sub_thread:
            if not group:
                return InlineKeyboardButton('✅ Notifiche', callback_data='%s:thread_sub:%s'%(thread_id, user_id))
            else:
                return InlineKeyboardButton('✅📬', callback_data='%s:thread_sub:%s:%s:group'%(thread_id, user_id, post_id))
        else:
            if not group:
                return InlineKeyboardButton('❌ Notifiche', callback_data='%s:thread_sub:%s'%(thread_id, user_id))
            else:
                return InlineKeyboardButton('❌📪', callback_data='%s:thread_sub:%s:%s:group'%(thread_id, user_id, post_id))
    else:
        return InlineKeyboardButton('🚫', callback_data='not_in_bot')
    


######################
### INLINE functions # 
######################

def getTagList(tags):
    lv1 = ""
    lv2 = ""
    text = ""
    for tag in tags.split(':'):
        tag_data = rdb.hgetall('tag:data:%s'%tag)
        slug = tag_data['name']
        if tag_data['homepage'] == '1':
            lv1 = "%s · %s"%(lv1, slug)
        elif tag_data['child'] == '1':
            lv2 = "%s · %s"%(lv2, slug)
        else:
            text = "%s · %s"%(text, slug)
    return lv1 + lv2 + text+' ·'

def getAnteprima(post_id):
    content = rdb.hget('post:data:%s'%post_id, 'content').lstrip()
    content = '\n'.join(content.split('\n\n'))
    pic_url = rdb.hget('post:data:%s'%post_id, 'pic_url')
    if len(content) > 318:
        return content[:320] + ' ...**continua a leggere sul forum**', pic_url
    else:
        return content.rstrip(), pic_url

def getUrlPic(post_id):
    return rdb.hget('post:data:%s'%post_id, 'pic_url')

def getInlineDocument(data, user_id):
    user_name = rdb.hget('user:data:%s'%data.main_user, 'username')
    user_photo = rdb.hget('user:data:%s'%data.main_user, 'avatar')
    thread_data = rdb.hgetall('thread:data:%s'%data.id)
    tag_list = thread_data['tags']
    title = thread_data['title']
    cd = thread_data['created_date']
    created_date = cd[8:10]+'/'+cd[5:7]+'/'+cd[2:4]
    posts_count = thread_data['comment_count']
    if user_photo == 'None':
        user_photo = False
    url_pic = getUrlPic(data.main_post)
    if url_pic:
        text = "[💡](%s) **%s**\n✍️ da `%s`\n\n%s\n\n🖇 __%s__"%(url_pic, title, user_name, getAnteprima(data.main_post)[0], getTagList(tag_list))
    else:
        text = "💡 **%s**\n✍️ da `%s`\n\n%s\n\n🖇 __%s__"%(title, user_name, getAnteprima(data.main_post)[0], getTagList(tag_list))

    return InlineQueryResultArticle(
            title=title,
            input_message_content=InputTextMessageContent(text),
            description='da %s · %s · %s'%(user_name, posts_count, created_date),
            thumb_url = user_photo,
            reply_markup=InlineKeyboardMarkup(
                [[InlineKeyboardButton('🌐Apri sul forum', url=data.url )]])
                                )


def getInlineArticle(data, user_id, vith=False):
    user_name = rdb.hget('user:data:%s'%data['main_user'], 'username')
    user_photo = rdb.hget('user:data:%s'%data['main_user'], 'avatar')
    thread_data = rdb.hgetall('thread:data:%s'%data['id'])
    tag_list = thread_data['tags']
    cd = thread_data['created_date']
    created_date = cd[8:10]+'/'+cd[5:7]+'/'+cd[2:4]
    posts_count = thread_data['comment_count']
    if user_photo == 'None':
        user_photo = False
    url_pic = getUrlPic(data['main_post'])
    if url_pic:
        text = "[💡](%s) **%s**\n✍️ da `%s`\n\n%s\n\n🖇 __%s__"%(url_pic, data['title'], user_name, getAnteprima(data['main_post'])[0], getTagList(tag_list))
    else:
        text = "💡 **%s**\n✍️ da `%s`\n\n%s\n\n🖇 __%s__"%(data['title'], user_name, getAnteprima(data['main_post'])[0], getTagList(tag_list))

    if not vith:
        return InlineQueryResultArticle(
            title=data['title'],
            input_message_content=InputTextMessageContent(text),
            description='da %s · %s · %s'%(user_name, posts_count, created_date),
            thumb_url = user_photo,
            reply_markup=InlineKeyboardMarkup(
                [[InlineKeyboardButton('🌐 Apri sul forum', url=data['url'] )]])
                                    )
    else:
        return InlineQueryResultArticle(
            title=data['title'],
            input_message_content=InputTextMessageContent(text),
            description='da %s · %s · %s'%(user_name, posts_count, created_date),
            thumb_url = user_photo,
            reply_markup=InlineKeyboardMarkup(
                [[InlineKeyboardButton('🌐 Apri sul forum', url=data['url'] ),isSubThread(user_id, data['id'])]])
            )

def getAllLastThread(user_id, vith=False):
    lista = []
    results = []
    for thread in rdb.keys('thread:data:*'):
        toapp = rdb.hgetall(thread)
        toapp['id'] = thread.split(':')[2]
        lista.append(toapp)
    sort_list = sorted(lista, key=lambda k: k['last_post_date'], reverse=True)
    for data in sort_list:
        results.append(getInlineArticle(data, user_id, vith))
    return results


##################
### CB functions #
##################

def getCategories(user_id):
    tags = rdb.keys('tag:data:*')
    ctgrs = []
    if rdb.exists('user:tg:%s'%user_id):
        sub_tag = set(rdb.hget('user:tg:%s'%user_id, 'sub_tag').split(':'))
    else:
        sub_tag = set([])
    if 'all' in sub_tag:
        sub_flag = '🌍✅'
    else:
        sub_flag = '❌'
    ctgrs.append([InlineKeyboardButton("%s· Iscrizione globale"%sub_flag, callback_data="%s:tag:all:precat")])
    tags.sort()
    for tag in tags:
        if toBool(rdb.hget(tag, 'homepage')):
            tdata = rdb.hgetall(tag)
            ctgrs.append([InlineKeyboardButton('🖇 %s'%tdata['name'], callback_data="%s:ctgrs:%s"%(user_id, tag.split(':')[2]))])
            
    ctgrs.append([InlineKeyboardButton("🌐 Forum", url="https://forum.eticadigitale.org"), InlineKeyboardButton('◀️ Indietro', callback_data="id:tab:expand")])
    return InlineKeyboardMarkup(ctgrs)

def getTagFromCtgrs(user_id, ctgrs_id):
    tags = rdb.keys('tag:data:*')
    tag_ctgrs = []

    if rdb.exists('user:tg:%s'%user_id):
        sub_tag = set(rdb.hget('user:tg:%s'%user_id, 'sub_tag').split(':'))
    else:
        sub_tag = set([])

    if ctgrs_id in sub_tag:
        sub_flag = '✅'
        sub_edit = True
    else:
        sub_flag = '❌'
        sub_edit = False
        
    tag_ctgrs.append([InlineKeyboardButton('%s· Tutta la categoria'%sub_flag, callback_data="%s:tag:%s:cat"%(user_id, ctgrs_id))])
    sub_flag = '☑️'
    for tag in tags:
        if tag.split(':')[2] in sub_tag and not sub_edit:
            sub_flag = '✅'
        elif not sub_edit:
            sub_flag = '❌'
        if rdb.hget(tag, 'parent_type') == ctgrs_id:
            tdata = rdb.hgetall(tag)
            tag_ctgrs.append([InlineKeyboardButton('%s· %s'%(sub_flag,tdata['name']), callback_data="%s:tag:%s:%s"%(user_id, tag.split(':')[2], ctgrs_id ))])
    tag_ctgrs.append([InlineKeyboardButton("🌐 Forum", url="https://forum.eticadigitale.org"),
                 InlineKeyboardButton('◀️ Indietro', callback_data="%s:tab:categories"%user_id)])
    return InlineKeyboardMarkup(tag_ctgrs)


def getAllTag(user_id, page=0): 
    page = int(page)
    tags = rdb.keys('tag:data:*')
    all_tag = []
    if rdb.exists('user:tg:%s'%user_id):
        sub_tag = set(rdb.hget('user:tg:%s'%user_id, 'sub_tag').split(':'))
        if 'all' in sub_tag:
            all_flag = True
        else:
            all_flag = False
    else:
        sub_tag = set([])

    et_list = []
    ha_list = []
    data_list = {}
    for tag in tags:
        name = rdb.hget(tag,'name')
        if name[0] == '#':
            data_list[name] = tag
            ha_list.append(name)
        else:
            data_list[name] = tag
            et_list.append(name)

    ha_list = sorted(ha_list)
    et_list = sorted(et_list)

    fi_list = et_list + ha_list

    double_tag = []

    item = 12*2
    pmin, pmax = page*item, page*item+item
    extra = 0

    if len(fi_list) < pmax:
        next_page = False
        extra = 1
    else:
        next_page = True

    if pmin == 0:
        first_page = True
        extra = 1
    else:
        first_page = False
        pmin+=1

    for count, tag in enumerate(fi_list[pmin:pmax+extra]):
        
        tag = data_list[tag]
        if not toBool(rdb.hget(tag, 'homepage')):          
            if not all_flag:
                if rdb.hget(tag, 'parent_type') in sub_tag:
                    sub_flag = '☑️'
                elif tag.split(':')[2] in sub_tag:
                    sub_flag = '✅'
                else:
                    sub_flag = '❌'
            else:
                sub_flag = '🌍☑️'
            tdata = rdb.hgetall(tag)
            if not len(double_tag) == 2:
                double_tag.append(InlineKeyboardButton('%s· %s'%(sub_flag, tdata['name']), callback_data="%s:tag:%s:all:%s"%(user_id, tag.split(':')[2], page)))
            else:
                all_tag.append(double_tag)
                double_tag = []
                double_tag.append(InlineKeyboardButton('%s· %s'%(sub_flag, tdata['name']), callback_data="%s:tag:%s:all:%s"%(user_id, tag.split(':')[2], page)))
    all_tag.append(double_tag)


    if next_page and first_page:
        all_tag.append([InlineKeyboardButton('➡️', callback_data='%s:tagmenu:%s'%(user_id, page+1))])
    elif next_page and not first_page:
        all_tag.append([InlineKeyboardButton('⬅️', callback_data="%s:tagmenu:%s"%(user_id, page-1)), InlineKeyboardButton('➡️', callback_data='%s:tagmenu:%s'%(user_id, page+1))])
    elif not next_page and not first_page:
        all_tag.append([InlineKeyboardButton('⬅️', callback_data='%s:tagmenu:%s'%(user_id, page-1))])

    all_tag.append([InlineKeyboardButton("🌐 Forum", url="https://forum.eticadigitale.org"),
                 InlineKeyboardButton('◀️ Indietro', callback_data="id:tab:expand")])
    return InlineKeyboardMarkup(all_tag)

                


def getStartReply(user_id):
    main_start = InlineKeyboardMarkup(
        [
            [InlineKeyboardButton("🗂 Categorie", callback_data="%s:tab:categories"%user_id), InlineKeyboardButton("📎 Etichette", callback_data="%s:tab:tags"%user_id)],
            [InlineKeyboardButton("ℹ️ Informazioni", callback_data="%s:tab:information"%user_id), InlineKeyboardButton("🔎 Cerca", switch_inline_query_current_chat="")],
            [InlineKeyboardButton("🌐 Forum", url="https://forum.eticadigitale.org")]
        ])
    return main_start



def getInformation(user_id):
    if rdb.exists('user:tg:%s'%user_id):
        sub_tag = set(rdb.hget('user:tg:%s'%user_id, 'sub_thread').split(':'))
    else:
        sub_tag = set([])
    if 'all' in sub_tag:
        sub_flag = '🌍✅'
    else:
        sub_flag = '❌'

    return InlineKeyboardMarkup(
        [ [ InlineKeyboardButton('off', callback_data="delay:tab:off"),  InlineKeyboardButton('1h', callback_data="delay:tab:1h"),  InlineKeyboardButton('2h', callback_data="delay:tab:2h"),  InlineKeyboardButton('4h', callback_data="delay:tab:4h")],
            [InlineKeyboardButton("%s· Notifica tutte le nuove risposte"%sub_flag, callback_data="%s:thread:all:precat")],
            [InlineKeyboardButton("🌐 Forum", url="https://forum.eticadigitale.org"),
                 InlineKeyboardButton('◀️ Indietro', callback_data="id:tab:superexpand")]

        ])
