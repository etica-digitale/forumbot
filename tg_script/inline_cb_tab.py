# SPDX-FileCopyrightText: 2021 Andrea Laisa <amreo linux it>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from pyrogram import Client, filters
from pyrogram.types import ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton
from zlibz.telegram_lib import *


def off_ifGroupIsAdmin(data):
    async def menu_filter(flt, _, query):
        return True
    return filters.create(menu_filter, data=data)

async def ifGroupIsAdmin(_, __, query):
    #print(query)
    if not query.from_user.id == query.message.chat.id:
        if str(query.from_user.id) in rdb.lrange('group:admin:%s'%query.message.chat.id,0,-1):
            return True
        else:
            await query.answer('Non è per te', show_alert=True)
            return False
    return True


@Client.on_callback_query(filters.regex('id:tab:expand') & filters.create(ifGroupIsAdmin))
async def expand(client, cbq):
    await cbq.edit_message_reply_markup(reply_markup=getStartReply(cbq.message.chat.id))
    await cbq.answer('', show_alert=False)
    return

@Client.on_callback_query(filters.regex('.+:settings:.+:.+') & filters.create(ifGroupIsAdmin))
async def group_settings(client, cbq):
    user_id = cbq.data.split(':')[0]
    thread_id = cbq.data.split(':')[2]
    post_id = cbq.data.split(':')[3]
    await cbq.edit_message_reply_markup(reply_markup=InlineKeyboardMarkup(
        [[InlineKeyboardButton('🔙', callback_data='%s:grpback:%s:%s'%(user_id, thread_id, post_id)), InlineKeyboardButton('🧹', callback_data='%s:grpclean:%s:%s'%(user_id, thread_id, post_id)), isNotifySubThread(user_id, thread_id, True, post_id)]]
        )) 
    await cbq.answer('', show_alert=False)
    return

@Client.on_callback_query(filters.regex('.+:grpback:.+:.+') & filters.create(ifGroupIsAdmin))
async def group_back(client, cbq):
    user_id = cbq.data.split(':')[0]
    thread_id = cbq.data.split(':')[2]
    post_id = cbq.data.split(':')[3]
    if post_id == 'none':
        url = rdb.hget('thread:data:%s'%thread_id, 'url')
    else:
        url = rdb.hget('thread:data:%s'%thread_id, 'url') + '/' + post_id
    await cbq.edit_message_reply_markup(reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton('🌐Apri sul forum', url=url ),InlineKeyboardButton('⚙️',callback_data='%s:settings:%s:%s'%(user_id,thread_id, post_id)) ]]))
    await cbq.answer('', show_alert=False)
    return

@Client.on_callback_query(filters.regex('.+:grpclean:.+:.+') & filters.create(ifGroupIsAdmin))
async def group_clean(client, cbq):
    user_id = cbq.data.split(':')[0]
    thread_id = cbq.data.split(':')[2]
    post_id = cbq.data.split(':')[3]
    if post_id == 'none':
        url = rdb.hget('thread:data:%s'%thread_id, 'url')
    else:
        url = rdb.hget('thread:data:%s'%thread_id, 'url') + '/' + post_id
    await cbq.edit_message_reply_markup(reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton('🌐Apri sul forum', url=url ) ]]))
    await cbq.answer('', show_alert=False)
    return

@Client.on_callback_query(filters.regex('delay:tab:.+') & filters.create(ifGroupIsAdmin))
async def set_delay(client, cbq):
    cb_value = cbq.data.split(':')[2]
    if cb_value == 'off':
        value = 1
    elif cb_value == '1h':
        value = 60*60
    elif cb_value == '2h':
        value = 60*60*2
    elif cb_value == '4h':
        value = 60*60*4
    rdb.hset('user:tg:%s'%cbq.message.chat.id, 'plus_time', value)
    await cbq.answer('Ritardo notifiche impostato a %s'%cb_value, show_alert=True)
    await cbq.answer('', show_alert=False)
    return

@Client.on_callback_query(filters.regex('id:tab:superexpand') & filters.create(ifGroupIsAdmin))
async def superexpand(client, cbq):
    await cbq.edit_message_text(main_text, reply_markup=getStartReply(cbq.message.chat.id))
    await cbq.answer('', show_alert=False)
    return

@Client.on_callback_query(filters.regex('.+:tab:categories') & filters.create(ifGroupIsAdmin))
async def categories(client, cbq):
    await cbq.edit_message_reply_markup(reply_markup=getCategories(cbq.message.chat.id))
    await cbq.answer('', show_alert=False)
    return

@Client.on_callback_query(filters.regex('.+:tab:tags') & filters.create(ifGroupIsAdmin))
async def tags(client, cbq):
    await cbq.edit_message_reply_markup(reply_markup=getAllTag(cbq.message.chat.id))
    await cbq.answer('', show_alert=False)
    return

@Client.on_callback_query(filters.regex('.+:tab:information') & filters.create(ifGroupIsAdmin))
async def information(client, cbq):
    testo = information_text
    await cbq.edit_message_text(testo, reply_markup=getInformation(cbq.message.chat.id))
    await cbq.answer('', show_alert=False)
    return

@Client.on_callback_query(filters.regex('.+:ctgrs:.+') & filters.create(ifGroupIsAdmin))
async def tag_from_ctgrs(client, cbq):
    await cbq.edit_message_reply_markup(reply_markup=getTagFromCtgrs(cbq.message.chat.id, cbq.data.split(':')[2]))
    await cbq.answer('', show_alert=False)
    return

@Client.on_callback_query(filters.regex('not_in_bot') & filters.create(ifGroupIsAdmin))
async def not_in_bot(client, cbq):
   print('! not in bot')
   return
   
@Client.on_callback_query(filters.regex('.+:inline_thread_sub:.+') & filters.create(ifGroupIsAdmin))
async def change_inline_thread_sub(client, cbq):
    if rdb.exists('user:tg:%s'%cbq.from_user.id) and str(cbq.from_user.id) == cbq.data.split(':')[2]:
        sub_thread = rdb.hget('user:tg:%s'%cbq.from_user.id, 'sub_thread').split(':')
        if cbq.data.split(':')[0] in sub_thread:
            sub_thread.remove(cbq.data.split(':')[0])
        else:
            sub_thread.append(cbq.data.split(':')[0])
        sub_thread = ':'.join(sub_thread)
        rdb.hset('user:tg:%s'%cbq.from_user.id, 'sub_thread', sub_thread)
        url = rdb.hget('thread:data:%s'%cbq.data.split(':')[0], 'url')
        await cbq.edit_message_reply_markup(reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton('🌐 Apri sul forum', url=url ),isSubThread(cbq.from_user.id, cbq.data.split(':')[0])]]))
        await cbq.answer('', show_alert=False)
    else:
        await cbq.answer('Questo pulsante non è per te.', show_alert=True)
    return

@Client.on_callback_query(filters.regex('.+:thread_sub:.+') & filters.create(ifGroupIsAdmin))
async def change_thread_sub(client, cbq):
    if rdb.exists('user:tg:%s'%cbq.message.chat.id) and str(cbq.message.chat.id) == cbq.data.split(':')[2]:
        sub_thread = rdb.hget('user:tg:%s'%cbq.message.chat.id, 'sub_thread').split(':')
        if cbq.data.split(':')[0] in sub_thread:
            sub_thread.remove(cbq.data.split(':')[0])
        else:
            sub_thread.append(cbq.data.split(':')[0])
        sub_thread = ':'.join(sub_thread)
        rdb.hset('user:tg:%s'%cbq.message.chat.id, 'sub_thread', sub_thread)
        url = rdb.hget('thread:data:%s'%cbq.data.split(':')[0], 'url')
        if len(cbq.data.split(':')) > 4 and cbq.data.split(':')[4] == 'group':
            user_id = cbq.message.chat.id
            thread_id = cbq.data.split(':')[0]
            post_id = cbq.data.split(':')[3]
            await cbq.edit_message_reply_markup(reply_markup=InlineKeyboardMarkup(
                [[InlineKeyboardButton('🔙', callback_data='%s:grpback:%s:%s'%(cbq.message.chat.id, cbq.data.split(':')[0], post_id)), InlineKeyboardButton('🧹', callback_data='%s:grpclean:%s:%s'%(user_id, thread_id, post_id)), isNotifySubThread(user_id, thread_id, True, post_id)]]))
        else: 
            await cbq.edit_message_reply_markup(reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton('🌐 Apri sul forum', url=url ),isNotifySubThread(cbq.message.chat.id, cbq.data.split(':')[0])]]))
        await cbq.answer('', show_alert=False)
    else:
        await cbq.answer('Questo pulsante non è per te.', show_alert=True)
    return

@Client.on_callback_query(filters.regex('.+:tag:.+:.+') & filters.create(ifGroupIsAdmin))
async def change_tag_sub(client, cbq):
    if rdb.exists('user:tg:%s'%cbq.message.chat.id):
        sub_tag = rdb.hget('user:tg:%s'%cbq.message.chat.id, 'sub_tag').split(':')
        if cbq.data.split(':')[2] in sub_tag:
            sub_tag.remove(cbq.data.split(':')[2])
        else:
            sub_tag.append(cbq.data.split(':')[2])
        sub_tag = ':'.join(sub_tag)
        rdb.hset('user:tg:%s'%cbq.message.chat.id, 'sub_tag', sub_tag)
        try:
         if cbq.data.split(':')[3] == 'cat':
            await cbq.edit_message_reply_markup(reply_markup=getTagFromCtgrs(cbq.message.chat.id, cbq.data.split(':')[2]))
            await cbq.answer('', show_alert=False)
            return
         if cbq.data.split(':')[3] == 'precat':
            await cbq.edit_message_reply_markup(reply_markup=getCategories(cbq.message.chat.id))
            await cbq.answer('', show_alert=False)
            return
         if not cbq.data.split(':')[3] == 'all':
            await cbq.edit_message_reply_markup(reply_markup=getTagFromCtgrs(cbq.message.chat.id, cbq.data.split(':')[3]))
            await cbq.answer('', show_alert=False)
            return
         else:
            await cbq.edit_message_reply_markup(reply_markup=getAllTag(cbq.message.chat.id, cbq.data.split(':')[4]))
            await cbq.answer('', show_alert=False)
            return
        except Exception as err:
                print(err)
                await cbq.answer("richiesta bloccata, ℹ️ per Informazioni.", show_alert=False)
                return

@Client.on_callback_query(filters.regex('.+:thread:.+:.+') & filters.create(ifGroupIsAdmin))
async def change_thread_not_sub(client, cbq):
    if rdb.exists('user:tg:%s'%cbq.message.chat.id):
        sub_tag = rdb.hget('user:tg:%s'%cbq.message.chat.id, 'sub_thread').split(':')
        if cbq.data.split(':')[2] in sub_tag:
            sub_tag.remove(cbq.data.split(':')[2])
        else:
            sub_tag.append(cbq.data.split(':')[2])
        sub_tag = ':'.join(sub_tag)
        rdb.hset('user:tg:%s'%cbq.message.chat.id, 'sub_thread', sub_tag)
        if cbq.data.split(':')[3] == 'cat':
            await cbq.edit_message_reply_markup(reply_markup=getTagFromCtgrs(cbq.message.chat.id, cbq.data.split(':')[2]))
            await cbq.answer('', show_alert=False)
            return
        if cbq.data.split(':')[3] == 'precat':
            await cbq.edit_message_reply_markup(reply_markup=getInformation(cbq.message.chat.id))
            await cbq.answer('', show_alert=False)
            return
        if not cbq.data.split(':')[3] == 'all':
            await cbq.edit_message_reply_markup(reply_markup=getTagFromCtgrs(cbq.message.chat.id, cbq.data.split(':')[3]))
            await cbq.answer('', show_alert=False)
            return
        else:
            await cbq.edit_message_reply_markup(reply_markup=getAllTag(cbq.message.chat.id))
            await cbq.answer('', show_alert=False)
            return

@Client.on_callback_query(filters.regex('.+:tagmenu:.+') & filters.create(ifGroupIsAdmin))
async def change_tag_page(client, cbq):
    await cbq.edit_message_reply_markup(reply_markup=getAllTag(cbq.message.chat.id, cbq.data.split(':')[2]))
    await cbq.answer('', show_alert=False)
    return

