# SPDX-FileCopyrightText: 2021 Andrea Laisa <amreo linux it>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from pyrogram import *
from pyrogram.types import *
from zlibz.zutils import * #?
from zlibz.telegram_lib import *

@Client.on_inline_query()
def get_and_reply(client, query):
    sq = query.query
    if query.query == "":
        results = getAllLastThread(query.from_user.id) 
        query.answer(results, cache_time=1)
        return 
    elif query.query == '-':
        results = getAllLastThread(query.from_user.id, True) 
        query.answer(results, cache_time=1)
        return
    q = Query(sq+'*').limit_fields('title').paging(0,50)
    res = rcli.search(q)
    allres = res.docs
    results = []
    if len(allres)>0:
      for data in allres:
        results.append(getInlineDocument(data, query.from_user.id))
      query.answer(results, cache_time=1)
      return
    else:
      results=[]
      results.append(InlineQueryResultArticle(
            title = sq,
            input_message_content=InputTextMessageContent('**404 not found**\n%s'%sq),
            description = 'NOT FOUND',
            ))
      query.answer(results, cache_time=0)
      return
