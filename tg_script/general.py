# SPDX-FileCopyrightText: 2021 Andrea Laisa <amreo linux it>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from pyrogram import Client, filters
import asyncio
from pyrogram.types import ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton
from zlibz.telegram_lib import *
from zlibz.zutils import sudo_user, bot_username


@Client.on_message(filters.command(['update']) & filters.user(sudo_user))
async def force_update(client, message):
    rdb.delete('update:block:tags')
    rdb.delete('update:block:threads')
    #rdb.delete('update:block:posts')
    #rdb.delete('update:block:groups')
    mex = await message.reply('ok, entro 90 secondi\nappena inizia modifico questo messaggio')
    rdb.set('force:update:%s'%message.from_user.id, mex.message_id)
    return

# admin update e propagazione comandi per gruppi 
@Client.on_message(filters.command(['start','start@%s'%bot_username, 'help','help@%s'%bot_username,'notifica', 'notifica@%s'%bot_username]) & filters.group)
async def group_check(client, message):
    admin_list = rdb.lrange('group:admin:%s'%message.chat.id, 0, -1)
    if not rdb.exists('group:admin:%s'%message.chat.id):
        admins = await client.get_chat_members(message.chat.id, filter='administrators')
        for admin in admins:
            if not str(admin.user.id) in admin_list:
                rdb.lpush('group:admin:%s'%message.chat.id, admin.user.id)
        rdb.setex('admin:block:%s'%message.chat.id, 60*60*6, 1)
    elif not rdb.exists('admin:block:%s'%message.chat.id):
        admins = await client.get_chat_members(message.chat.id, filter='administrators')
        for admin in admins:
            if not str(admin.user.id) in admin_list:
                rdb.lpush('group:admin:%s'%message.chat.id, admin.user.id)
        rdb.setex('admin:block:%s'%message.chat.id, 60*60*6, 1)
    admin_list = rdb.lrange('group:admin:%s'%message.chat.id, 0, -1)
    if str(message.from_user.id) in admin_list:
        message.continue_propagation()
    else:
        message.delete()
        return

@Client.on_message(filters.command(['notifica', 'notifica@%s'%bot_username]) )
async def notifica_flag(client, message):
    url = message.reply_to_message.reply_markup.inline_keyboard[0][0].url
    thread_id = getThreadInlineUrl(url)

    if rdb.exists('user:tg:%s'%message.chat.id):
        sub_thread = rdb.hget('user:tg:%s'%message.chat.id, 'sub_thread').split(':')
        title = rdb.hget('thread:data:%s'%thread_id, 'title')
        if thread_id in sub_thread:
            sub_thread.remove(thread_id)
            mex = await message.reply('Le notifiche della discussione:\n__%s__\nsono  ❌ **disabilitate**.'%title)
        else:
            sub_thread.append(thread_id)
            mex = await message.reply('Le notifiche della discussione:\n__%s__\nsono ✅ **abilitate**.'%title)
        sub_thread = ':'.join(sub_thread)
        rdb.hset('user:tg:%s'%message.chat.id, 'sub_thread', sub_thread)
        await asyncio.sleep(7)
        await client.delete_messages(message.chat.id, mex.message_id)
        await client.delete_messages(message.chat.id, message.message_id)

    return

@Client.on_message(filters.command(['start', 'start@%s'%bot_username ,'help', 'help@%s'%bot_username]) )
async def welcome(client, message):
    if not rdb.exists('user:tg:%s'%message.chat.id):
        rdb.hmset('user:tg:%s'%message.chat.id, {
                            'sub_tag' : '',
                            'muted_tag' : '',
                            'sub_thread' : '',
                            'muted_thread' : '',
                            'plus_time' : 2*60*60
                                                }
                )
    first_name = message.from_user.first_name
    await message.reply(main_text, reply_markup=getStartReply(message.chat.id))
