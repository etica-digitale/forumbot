# SPDX-FileCopyrightText: 2021 Andrea Laisa <amreo linux it>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from zlibz.zutils import *
from zlibz.telegram_lib import *
from pyrogram.types import ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton
from zlibz.redisUpdate import *
import time
from uuid import uuid4
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.schedulers.background import BackgroundScheduler


global not_boot
not_boot = False


def main():
    global not_boot
    print(not_boot)
    for force in rdb.keys('force:update:*'):
        bot.edit_message_text(force.split(':')[2], int(rdb.get(force)), 'Inizio ora..')
        
    # update:block:threads
    # update:block:tags
    # update:block:groups
    # update:block:posts

# update all threads
    if not rdb.exists('update:block:threads'):
        if update_all_threads(new=not_boot): 
            rdb.setex('update:block:threads', upd_threads_ttl, 1)
            print('- update all threads done!')

# update all tags
    if not rdb.exists('update:block:tags'):
        if update_all_tags():
            rdb.setex('update:block:tags', upd_tags_ttl, 1)
            print('- update all tags, done!')

# update all groups
    if not rdb.exists('update:block:groups'):
        if update_all_groups():
            rdb.setex('update:block:groups', upd_groups_ttl, 1)
            print('- update all groups, done!')

# update all posts
    if not rdb.exists('update:block:posts'):
        if update_all_posts_from_threads():
            rdb.setex('update:block:posts', upd_posts_from_thread_ttl, 1)
            print('- update all posts from threads, done!')

# check new threads added by update_all_thread()
# create the thread_notify for sub users
    for thread in rdb.keys('thread:new:*'):
        tags = rdb.get(thread).split(':')
        for user in rdb.keys('user:tg:*'):
            sub_tags = rdb.hget(user, 'sub_tag')
            if sub_tags:
                sub_tags = sub_tags.split(':')
            else:
                continue
            if set(tags) & set(sub_tags) or 'all' in sub_tags:
                uid = uuid4().hex[:25]
                rdb.hmset('user:thread_notify:%s'%uid, {
                                'thread' : thread.split(':')[2],
                                'tag' : rdb.get(thread),
                                'user_id' : user.split(':')[2]
                                                                }
                          )
                rdb.setex('notify:block:%s'%uid, rdb.hget(user, 'plus_time'), 1)
        rdb.delete(thread)

# check new posts added by update_all_thread() and update_all_posts_from_threads()
# create the post_notify for sub users
    for post in rdb.keys('post:new:*'):
        thread = rdb.get(post)
        for user in rdb.keys('user:tg:*'):
            sub_threads = rdb.hget(user, 'sub_thread')
            if sub_threads:
                sub_threads = sub_threads.split(':')
            else:
                continue
            if set([thread]) & set(sub_threads) or 'all' in sub_threads:
                uid = uuid4().hex[:25]
                rdb.hmset('user:post_notify:%s'%uid, {
                                    'thread' : thread,
                                    'post'   : post.split(':')[2],
                                    'user_id': user.split(':')[2],
                                    'comment_count' : rdb.hget('thread:data:%s'%thread, 'comment_count')
                    })
                rdb.setex('notify:block:%s'%uid, rdb.hget(user, 'plus_time'), 1)
        rdb.delete(post)


    # per ricevere subito le notifiche con il plus_time minimo di 1 secondo ( off )
    time.sleep(1.1)

# check new thread_notify and check if they are blocked
# if not, send
    if len(rdb.keys('user:thread_notify:*')) > 0:
            for notify in rdb.keys('user:thread_notify:*'):
                uid = notify.split(':')[2]
                notify_data = rdb.hgetall(notify)
                sub_tags = rdb.hget('user:tg:%s'%notify_data['user_id'], 'sub_tag').split(':')
                tags = notify_data['tag'].split(':')
                if not rdb.exists('notify:block:%s'%uid) :
# se il thread della notifica è ancora in sub_tag o se esiste 'all', se non esiste il blocco, se esistono ancora i dati del thread nel db
                    if set(tags) & set(sub_tags) or 'all' in sub_tags and rdb.exists('thread:data:%s'%notify_data['thread']): 
                        thread = rdb.hgetall('thread:data:%s'%notify_data['thread'])
                        title = thread['title']
                        url = thread['url']
                        user_name = rdb.hget('user:data:%s'%thread['main_user'], 'username')
                        content, url_pic = getAnteprima(thread['main_post'])
                        tags = getTagList(thread['tags'])
                        if url_pic:
                            text = "📬 __nuova discussione__\n[💡](%s) **%s**\n✍️ da `%s`\n\n%s\n\n🖇 __%s__"%(url_pic, title, user_name, content, tags)
                        else:
                            text = "📬 __nuova discussione__\n💡 **%s**\n✍️ da %s\n\n`%s`\n\n🖇 __%s__"%(title, user_name, content, tags)
                        try:
                            if notify_data['user_id'][:4] == '-100':
                                bot.send_message(notify_data['user_id'], text, reply_markup=InlineKeyboardMarkup(
                                    [[InlineKeyboardButton('🌐Apri sul forum', url=url )]] 
                                               ))
                            else:
                                bot.send_message(notify_data['user_id'], text, reply_markup=InlineKeyboardMarkup(
                                    [[InlineKeyboardButton('🌐Apri sul forum', url=url ),isNotifySubThread(notify_data['user_id'], notify_data['thread'])]]
                                               ))
                            rdb.delete(notify)
                        except Exception as err:
                            print(err)
                            rdb.delete(notify)
                    else:
                        rdb.delete(notify)

# check new post_notify and check if they are blocked
# if not, send
    if len(rdb.keys('user:post_notify:*')) > 0:
        for notify in rdb.keys('user:post_notify:*'):
            uid = notify.split(':')[2]
            notify_data = rdb.hgetall(notify)
            sub_threads = rdb.hget('user:tg:%s'%notify_data['user_id'], 'sub_thread').split(':')
            if not rdb.exists('notify:block:%s'%uid):
# se il post della notifica è ancora in sub_threads o se esiste 'all', se non esiste il blocco, se esistono ancora i dati del thread nel db
                if set([notify_data['thread']]) & set(sub_threads) or 'all' in sub_threads and rdb.exists('thread:data:%s'%notify_data['thread']) and rdb.exists('post:data:%s'%notify_data['post']):
                    thread = rdb.hgetall('thread:data:%s'%notify_data['thread'])
                    title = thread['title']
                    url = thread['url']+'/'+notify_data['comment_count'] #notify_data['post']
                    post = rdb.hgetall('post:data:%s'%notify_data['post'])
                    content, url_pic = getAnteprima(notify_data['post'])
                    user_name = rdb.hget('user:data:%s'%post['main_user'], 'username') 
                    tags = getTagList(thread['tags'])
                    if url_pic:
                        text = "📬 __nuova risposta__\n✍️ da `%s`\n[💡](%s) **%s**\n\n%s\n\n🖇 __%s__"%(user_name, url_pic, title, content, tags)
                    else:
                        text = "📬 __nuova risposta\n✍️ da `%s`\n💡 **%s**\n\n%s\n\n🖇 __%s__"%(user_name, title, content, tags)
                    try:
                        if notify_data['user_id'][:4] == '-100':
                            bot.send_message(notify_data['user_id'], text, reply_markup=InlineKeyboardMarkup(
                                [[InlineKeyboardButton('🌐Apri sul forum', url=url )]]
                                               ))
                        else:
                            bot.send_message(notify_data['user_id'], text, reply_markup=InlineKeyboardMarkup(
                                [[InlineKeyboardButton('🌐Apri sul forum', url=url ),isNotifySubThread(notify_data['user_id'], notify_data['thread'])]]
                                               ))
                        rdb.delete(notify)
                    except Exception as err:
                        print(err)
                        rdb.delete(notify)

                else:
                    rdb.delete(notify)



    if not not_boot:
        not_boot=True 

    print('all updated!')
    for force in rdb.keys('force:update:*'):
        bot.send_message(force.split(':')[2], '..finitO', reply_to_message_id=int(rdb.get(force)))
        rdb.delete(force)
    return


#scheduler = AsyncIOScheduler()
scheduler = BackgroundScheduler()
scheduler.add_job(main, "interval", seconds=90)
scheduler.start()

bot.run()


