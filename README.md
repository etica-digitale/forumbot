<!--
SPDX-FileCopyrightText: 2021 Andrea Laisa <amreo linux it>

SPDX-License-Identifier: CC0-1.0
-->

# ForumBot

Bot Telegram. Ponte tra https://forum.eticadigitale.org e qualsivoglia gruppo, nonché presente nel [gruppo ufficiale](https://t.me/eticadigitale) di Etica Digitale.
